<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/3/w3.css">
<body>

<h1># spring_instragram_clone</h1><br>

<h4>#Feature Of This project </h4><br>
<p>->User Register</p><br>
<p>->User Login</p><br>
<p>->User Can See Own Profile</p><br>
<p>->Edit Own Profile</p><br>
<p>->Add Post to his timeline</p><br>
<p>->Follow the users which are registered into this system</p><br>
<p>->User can comment the post</p><br>
<p>->User can like the post</p><br>

<h4>#Technologies are Used This Project</h4><br>
<p>->Spring MVC</p><br>
<p>->Spring Security</p><br>
<p>->Hibernate ORM</p><br>
<p>->JSP</p><br>

<h4>#Project Screenshots</h4><br>
<p align="center">
  <img src="https://gitlab.com/sharif.ahmed93/spring_instragram_clone/-/blob/master/screenshots/login.PNG" width="350" alt="LOGIN">
</p><br>
<p align="center">
  <img src="https://gitlab.com/sharif.ahmed93/spring_instragram_clone/-/blob/master/screenshots/reg.PNG" width="350" alt="REGISTER">
</p><br>
<p align="center">
  <img src="https://gitlab.com/sharif.ahmed93/spring_instragram_clone/-/blob/master/screenshots/home.PNG " width="350" alt="HOME PAGE">
</p><br>
<p align="center">
  <img src="https://gitlab.com/sharif.ahmed93/spring_instragram_clone/-/blob/master/screenshots/profile.PNG" width="350" alt="PROFILE">
</p><br>
<p align="center">
  <img src="https://gitlab.com/sharif.ahmed93/spring_instragram_clone/-/blob/master/screenshots/edit-profile.PNG" width="350" alt="EDIT PROFILE">
</p><br>
<!--![LOGIN](https://gitlab.com/sharif.ahmed93/spring_instragram_clone/-/blob/master/screenshots/login.PNG "LOGIN")
![REGISTER](https://gitlab.com/sharif.ahmed93/spring_instragram_clone/-/blob/master/screenshots/reg.PNG "REGISTER")
![DASH BOARD](https://gitlab.com/sharif.ahmed93/spring_instragram_clone/-/blob/master/screenshots/home.PNG "DASH BOARD")
![PROFILE](https://gitlab.com/sharif.ahmed93/spring_instragram_clone/-/blob/master/screenshots/profile.PNG "PROFILE")
![EDIT PROFILE](https://gitlab.com/sharif.ahmed93/spring_instragram_clone/-/blob/master/screenshots/edit-profile.PNG "EDIT PROFILE")-->
</body>
</html>




<!--<h1># spring_instragram_clone</h1>

#Feature Of This project 
->User Register
->User Login
->User Can See Own Profile
->Edit Own Profile
->Add Post to his timeline
->Follow the users which are registered into this system
->User can comment the post
->User can like the post

#Technologies are Used This Project
->Spring MVC
->Spring Security
->Hibernate ORM
->JSP

#Project Screenshots
<p align="center">
  <img src="https://gitlab.com/sharif.ahmed93/spring_instragram_clone/-/blob/master/screenshots/login.PNG" width="350" alt="LOGIN">
  <img src="https://gitlab.com/sharif.ahmed93/spring_instragram_clone/-/blob/master/screenshots/reg.PNG" width="350" alt="REGISTER">
</p>
![LOGIN](https://gitlab.com/sharif.ahmed93/spring_instragram_clone/-/blob/master/screenshots/login.PNG "LOGIN")
![REGISTER](https://gitlab.com/sharif.ahmed93/spring_instragram_clone/-/blob/master/screenshots/reg.PNG "REGISTER")
![DASH BOARD](https://gitlab.com/sharif.ahmed93/spring_instragram_clone/-/blob/master/screenshots/home.PNG "DASH BOARD")
![PROFILE](https://gitlab.com/sharif.ahmed93/spring_instragram_clone/-/blob/master/screenshots/profile.PNG "PROFILE")
![EDIT PROFILE](https://gitlab.com/sharif.ahmed93/spring_instragram_clone/-/blob/master/screenshots/edit-profile.PNG "EDIT PROFILE")-->
