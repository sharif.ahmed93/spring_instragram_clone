<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core_1_1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Home</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Instragram</a>
    <sec:authentication var="principal" property="principal"/>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
        </ul>
        <ul class="navbar-nav my-2 my-lg-0">
            <li class="nav-item">
                <a class="nav-link" href="<c:url value='/user/profile'/>">${principal.username}</a>
            </li>
        </ul>
    </div>
</nav>
<!-- /.navbar -->
<%--<div align="center">

${pageContext.request.contextPath}/user/profile/${principal.username}
    <form:form>
        <table border="0" id="customers">
            <tr>
                <td colspan="2" align="center"><h1>Instagram Home</h1></td>
            </tr>
            <tr>
                <td>
                    <a href="${pageContext.request.contextPath}/user/profile">View Profile</a>
                </td>
            </tr>
        </table>
    </form:form>
    <a href="02-ProfilePage.html" class="author-name fn">
        <div class="author-title">
            <sec:authentication var="principal" property="principal"/>
            ${principal.username}
            <svg class="olymp-dropdown-arrow-icon">
                <use xlink:href="svg-icons/sprites/icons.svg#olymp-dropdown-arrow-icon"></use>
            </svg>
        </div>
        <span class="author-subtitle"></span>
    </a>
</div>--%>

</body>
</html>
