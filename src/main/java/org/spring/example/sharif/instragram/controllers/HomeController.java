package org.spring.example.sharif.instragram.controllers;

import org.spring.example.sharif.instragram.dto.CommentDto;
import org.spring.example.sharif.instragram.dto.FollowDto;
import org.spring.example.sharif.instragram.dto.LikeDto;
import org.spring.example.sharif.instragram.dto.PostDto;
import org.spring.example.sharif.instragram.model.*;
import org.spring.example.sharif.instragram.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Controller
public class HomeController {

    @Autowired
    UserService userService;

    @Autowired
    PostService postService;

    @Autowired
    CommentService commentService;

    @Autowired
    FollowingService followingService;

    @Autowired
    LikeService likeService;

    @GetMapping("/user/post/show/all")
    public String getAllPost(Model model, Authentication authentication) {
        List<Post> postList = postService.getAllPost();
        List<Follow> followingUserList = followingService.getAllFollowingUserByCurrentUserId(userService.getUserByUserName(authentication.getName()).getUserId());
        List<User> userList = new ArrayList<>();
        List<List<Comment>> commentList = new ArrayList<>();
        List<String> totalCommentList = new ArrayList<>();
        List<String> totalLikeList = new ArrayList<>();
        for (Post post : postList) {
            long postId = post.getPostId();
            List<Comment> commentListByPostId = commentService.getAllCommentsByPostId(postId);
            String totalLikeOfThisPost = String.valueOf(likeService.getTotalLikeOfThisPostByPostId(postId));
            totalLikeList.add(totalLikeOfThisPost);
            commentList.add(commentListByPostId);
            totalCommentList.add(String.valueOf(commentListByPostId.size()));
        }
        for (User user : userService.getAllUsers()) {
            if (!user.getUsername().equalsIgnoreCase(authentication.getName())) {
                userList.add(user);
            }
        }
        model.addAttribute("postList", postList);
        model.addAttribute("totalLikeList", totalLikeList);
        model.addAttribute("userList", userList);
        model.addAttribute("commentList", commentList);
        model.addAttribute("followingUserList", followingUserList);
        model.addAttribute("totalCommentList", totalCommentList);
        model.addAttribute("message", "All Post List");
        model.addAttribute("postDto", new PostDto());
        model.addAttribute("followDto", new FollowDto());
        model.addAttribute("commentDto", new CommentDto());
        model.addAttribute("likeDto", new LikeDto());
        return "index";
    }
}
