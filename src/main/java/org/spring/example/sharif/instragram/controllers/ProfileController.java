package org.spring.example.sharif.instragram.controllers;

import org.spring.example.sharif.instragram.Constants;
import org.spring.example.sharif.instragram.dto.ProfileUpdateDto;
import org.spring.example.sharif.instragram.dto.UserDto;
import org.spring.example.sharif.instragram.model.Post;
import org.spring.example.sharif.instragram.model.User;
import org.spring.example.sharif.instragram.services.PostService;
import org.spring.example.sharif.instragram.services.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import javax.validation.Valid;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;

@Controller
public class ProfileController {

    @Autowired
    ServletContext servletContext;

    @Autowired
    UserService userService;

    @Autowired
    PostService postService;

    @GetMapping("/user/profile")
    public String viewProfile(Model model, Authentication authentication) {
        User user = userService.getUserByUserName(authentication.getName());
        List<Post> postListByLoginUser = postService.getAllPostByOwnerId(user.getUserId());
        UserDto userDto = new UserDto();
        BeanUtils.copyProperties(user, userDto);
        model.addAttribute("userDto", userDto);
        model.addAttribute("postListByLoginUser", postListByLoginUser);
        return "profile";
    }

    @GetMapping("/user/profile/edit/{id}")
    public String viewProfileEdit(Model model, @PathVariable("id") String id) {
        User user = userService.getUserByUserId(Long.parseLong(id));
        ProfileUpdateDto userDto = new ProfileUpdateDto();
        BeanUtils.copyProperties(user, userDto);
        model.addAttribute("userDto", userDto);
        return "edit-profile";
    }

    @PostMapping("/user/profile/edit")
    public String updateProfile(Model model, @Valid @ModelAttribute("userDto") ProfileUpdateDto userDto, @RequestParam("file") MultipartFile file, BindingResult bindingResult) {
        if (!bindingResult.hasErrors()) {
            User user = userService.getUserByUserName(userDto.getUsername());
            BeanUtils.copyProperties(userDto, user);
            String profileImageUrl = uploadFile(file);
            if (profileImageUrl != null && !profileImageUrl.isEmpty()) {
                user.setProfileImage(profileImageUrl);
            }
            userService.updateUserByUserId(user);
            model.addAttribute("message", "Profile Update Successfully!");
            return "redirect:/user/profile";
        }
        return "edit-profile";
    }

    public String uploadFile(MultipartFile file) {
        if (!file.isEmpty()) {
            try {
                String absolutePath = Constants.PROFILE_IMG_ABSOLUTE_PATH;
                String fileName = "pp" + new Date().getTime() + ".jpg";

                Path path = Paths.get(absolutePath);
                File directory = path.toFile();

                //servletContext.getRealPath("/WEB-INF/resources/images/profile/")
                //File dir = Paths.get(absoluteFilePath + userName + "//").toFile();

                if (!directory.exists()) {
                    boolean bool = directory.mkdirs();
                    if (bool) {
                        System.out.println("Directory created successfully");
                    } else {
                        System.out.println("Sorry couldn't create specified directory");
                    }
                }

                System.out.println("dir path: " + directory.getAbsolutePath());

                File outputFile = new File(directory, fileName);

                BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(outputFile));
                outputStream.write(file.getBytes());
                outputStream.flush();
                outputStream.close();

                //String outputImageUrl = directory.getAbsolutePath()+fileName;
                String outputImageUrl = "/images/profile/"+fileName;
                System.out.println("file name: " + outputFile.getName());
                //return outputFile.getName();
                return outputImageUrl;
            } catch (IOException e) {
                e.printStackTrace();
                return "";
            }
        } else {
            return "";
        }
    }


    @PostMapping("/user/uploadFile")
    public String uploadFiles(@RequestParam(name = "file") MultipartFile multipartFile, ModelMap modelMap) {


        User authenticateduser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        var username = authenticateduser.getUsername();
        User user = userService.getUserByUserName(authenticateduser.getUsername());
        String pictureName = "pp" + user.getUserId() + ".jpg";
        // Save file on system
        if (!multipartFile.getOriginalFilename().isEmpty()) {


            try {


                File directory = new File(servletContext.getRealPath("/WEB-INF/resources/images/profile/"));


                if (!directory.exists()) {
                    boolean bool = directory.mkdirs();
                    if (bool) {
                        System.out.println("Directory created successfully");
                    } else {
                        System.out.println("Sorry couldn't create specified directory");
                    }
                }

                System.out.println("dir path: " + directory.getAbsolutePath());

                File outputfile = new File(directory, pictureName);

                BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(outputfile));
                outputStream.write(multipartFile.getBytes());
                outputStream.flush();
                outputStream.close();

                //userService.updateuserProfilePicture("/images/profile/"+pictureName,username);

                System.out.println("file name: " + outputfile.getName());
                modelMap.addAttribute("fileName", outputfile.getName());
                modelMap.addAttribute("photo_uri", outputfile.getAbsolutePath());
                modelMap.addAttribute("msg", "File uploaded successfully.");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                modelMap.addAttribute("msg", "Failed to save file properly.");
            } catch (IOException e) {
                e.printStackTrace();
                modelMap.addAttribute("msg", "Failed to save file properly.");
            }

        } else {
            modelMap.addAttribute("msg", "Please select a valid file..");
        }


        // modelMap.addAttribute("file", multipartFile);

        return "redirect:/";

    }

}
