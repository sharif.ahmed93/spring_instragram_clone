package org.spring.example.sharif.instragram;

public class Constants {
    public static String PROFILE_IMG_ABSOLUTE_PATH = System.getProperty("user.home") + "/spring-instagram-clone/WEB-INF/resources/images/profile/";
    public static String POST_IMG_ABSOLUTE_PATH = System.getProperty("user.home") + "/spring-instagram-clone/WEB-INF/resources/images/post/";
}
