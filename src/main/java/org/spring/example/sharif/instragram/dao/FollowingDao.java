package org.spring.example.sharif.instragram.dao;

import org.spring.example.sharif.instragram.model.Follow;
import org.spring.example.sharif.instragram.model.Reaction;
import org.spring.example.sharif.instragram.model.User;

import java.util.List;

public interface FollowingDao {

    public void addFollowingUser(Follow follow);

    public List<Follow> getAllFollowingUserByCurrentUserId(long currentUserId);

    public boolean alreadyFollowTheUserByCurrentUserId(long currentUserId, long followingUserId);

    public Follow getFollowingUserByCurrentUserId(long currentUserId, long followingUserId);

    public void deleteFollowingUser(Follow follow);

}
