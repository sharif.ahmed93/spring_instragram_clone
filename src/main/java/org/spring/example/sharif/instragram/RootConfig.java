package org.spring.example.sharif.instragram;

import org.spring.example.sharif.instragram.config.persistence.HibernateConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;


@ComponentScan(basePackages = {
        "org.spring.example.sharif.instragram.config.security",
        "org.spring.example.sharif.instragram.services",
        "org.spring.example.sharif.instragram.dao",
})
public class RootConfig {
    @Bean
    GlobalExceptionHandler globalExceptionHandler() {
        return new GlobalExceptionHandler();
    }

    @Bean
    HibernateConfig hibernateConfig() {
        return new HibernateConfig();
    }

    @Bean
    public PasswordEncoder PasswordEncoder(){
        return  new BCryptPasswordEncoder();
    }

    @Bean
    public StandardServletMultipartResolver multipartResolver(){
        return new StandardServletMultipartResolver();
    }
}
