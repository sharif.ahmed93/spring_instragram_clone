package org.spring.example.sharif.instragram.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.spring.example.sharif.instragram.config.persistence.HibernateConfig;
import org.spring.example.sharif.instragram.model.Comment;
import org.spring.example.sharif.instragram.model.Post;
import org.spring.example.sharif.instragram.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class CommentDaoImpl implements CommentDao {

    @Autowired
    HibernateConfig hibernateConfig;

    @Override
    public void addComment(Comment comment) {
        Session session = hibernateConfig.getSession();
        Transaction tx = session.getTransaction();

        if (!tx.isActive()) {
            tx = session.beginTransaction();
        }

        try {
            session.save(comment);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();

        } finally {
            session.close();
        }
    }

    @Override
    public List<Comment> getAllComments() {
        Session session = hibernateConfig.getSession();
        Transaction tx = session.getTransaction();
        if (!tx.isActive()){
            tx = session.beginTransaction();
        }
        List<Comment> commentList = session.createQuery("from Comment").list();
        tx.commit();
        return commentList;
    }

    @Override
    public List<Comment> getAllCommentsByPostId(long postId) {
        Session session = hibernateConfig.getSession();
        Transaction tx = session.getTransaction();
        if (!tx.isActive()) {
            tx = session.beginTransaction();
        }
        //Team team = (Team) session.get(Team.class,id);
        /*CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Comment> teamCriteriaQuery = cb.createQuery(Comment.class);
        Root<Comment> root = teamCriteriaQuery.from(Comment.class);

        teamCriteriaQuery.where(cb.equal(root.get("postId"), postId));
        List<Comment> comments = session.createQuery(teamCriteriaQuery).getResultList();
        */

        var query = session.getEntityManagerFactory().createEntityManager()
                .createQuery("select c from Comment c where c.post.postId=:postId ", Comment.class);
        query.setParameter("postId", postId);
        List<Comment> comments = query.getResultList();

        tx.commit();
        return comments;
    }

    /*public Course getCourseByCourseCode(String courseCode) {

        var session = hibernateConfig.getSession();
        Transaction tx = session.getTransaction();
        if (!tx.isActive())
            tx = session.beginTransaction();

        var query = session.getEntityManagerFactory().createEntityManager()
                .createQuery("select c from Course c where courseCode=:courseCode ", Course.class);
        query.setParameter("courseCode", courseCode);
        Course course = query.getResultList().get(0);
        return course;
    }*/


}
