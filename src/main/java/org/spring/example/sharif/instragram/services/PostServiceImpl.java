package org.spring.example.sharif.instragram.services;

import org.spring.example.sharif.instragram.dao.PostDao;
import org.spring.example.sharif.instragram.dao.UserDao;
import org.spring.example.sharif.instragram.model.Post;
import org.spring.example.sharif.instragram.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostServiceImpl implements PostService {
    @Autowired
    PostDao postDao;

    @Override
    public void addPost(Post post) {
        postDao.addPost(post);
    }

    @Override
    public User getPostByUserNameAndOwnerId(String username, long ownerId) {
        return postDao.getPostByUserNameAndOwnerId(username, ownerId);
    }

    @Override
    public Post getPostByPostId(long postId) {
        return postDao.getPostByPostId(postId);
    }

    @Override
    public User updatePostByUserIdAndPostId(long userId, long postId) {
        return postDao.updatePostByUserIdAndPostId(userId, postId);
    }

    @Override
    public void updatePostByPostIdAndUserId(long postId, long userId) {
        postDao.updatePostByPostIdAndUserId(postId, userId);
    }

    @Override
    public List<Post> getAllPostByOwnerId(long postOwnerId) {
        return postDao.getAllPostByOwnerId(postOwnerId);
    }

    @Override
    public List<Post> getAllPostByPostDateTime(String dateTime) {
        return postDao.getAllPostByPostDateTime(dateTime);
    }

    @Override
    public List<Post> getAllPost() {
        return postDao.getAllPost();
    }
}
