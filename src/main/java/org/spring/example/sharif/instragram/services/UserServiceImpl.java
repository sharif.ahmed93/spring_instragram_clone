package org.spring.example.sharif.instragram.services;

import org.spring.example.sharif.instragram.dao.UserDao;
import org.spring.example.sharif.instragram.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;

    @Override
    public void addUser(User user) {
        userDao.addUser(user);
    }

    @Override
    public boolean isUserExists(String username) {
        return userDao.isUserExists(username);
    }

    @Override
    public User getUserByUserName(String username) {
        return userDao.getUserByUserName(username);
    }

    @Override
    public User getUserByUserId(long userId) {
        return userDao.getUserByUserId(userId);
    }

    @Override
    public void updateUserByUserId(User user) {
        userDao.updateUserByUserId(user);
    }

    @Override
    public List<User> getAllUsers() {
        return userDao.getAllUsers();
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        User user = getUserByUserName(username);
        if (user ==  null){
            throw  new UsernameNotFoundException("No user found with this email address.");
        }
        List<GrantedAuthority> authorities = new java.util.ArrayList<>();
        User finalUser = user;
        authorities.add((GrantedAuthority) () -> finalUser.getUsername());
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), authorities);
    }

    public void updateuserProfilePicture(String imageUrl,String username) {
        User user = userDao.getUserByUserName(username);

        user.setProfileImage(imageUrl);

        userDao.addUser(user);


    }


}
