package org.spring.example.sharif.instragram.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.spring.example.sharif.instragram.config.persistence.HibernateConfig;
import org.spring.example.sharif.instragram.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class FollowingDaoImpl implements FollowingDao {

    @Autowired
    HibernateConfig hibernateConfig;

    @Override
    public void addFollowingUser(Follow follow) {
        Session session = hibernateConfig.getSession();
        Transaction tx = session.getTransaction();

        if (!tx.isActive()) {
            tx = session.beginTransaction();
        }

        try {
            session.save(follow);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();

        } finally {
            session.close();
        }
    }

    @Override
    public List<Follow> getAllFollowingUserByCurrentUserId(long currentUserId) {
        Session session = hibernateConfig.getSession();
        Transaction tx = session.getTransaction();
        if (!tx.isActive()) {
            tx = session.beginTransaction();
        }
        //Team team = (Team) session.get(Team.class,id);
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Follow> teamCriteriaQuery = cb.createQuery(Follow.class);
        Root<Follow> root = teamCriteriaQuery.from(Follow.class);

        teamCriteriaQuery.where(cb.equal(root.get("currentUserId"), currentUserId));
        List<Follow> follows = session.createQuery(teamCriteriaQuery).getResultList();

        tx.commit();
        return follows;
    }

    @Override
    public boolean alreadyFollowTheUserByCurrentUserId(long currentUserId, long followingUserId) {
        Session session = hibernateConfig.getSession();
        Transaction tx = session.getTransaction();
        if (!tx.isActive()) {
            tx = session.beginTransaction();
        }

        var query = session.getEntityManagerFactory().createEntityManager()
                .createQuery("select f from Follow f where f.currentUserId=:curUserId and f.followingUser.userId=:followUserId", Follow.class);
        query.setParameter("curUserId", currentUserId);
        query.setParameter("followUserId", followingUserId);

        List<Follow> reactions = query.getResultList();

        tx.commit();
        return reactions.size() > 0 ? true : false;
    }

    @Override
    public Follow getFollowingUserByCurrentUserId(long currentUserId, long followingUserId) {
        Session session = hibernateConfig.getSession();
        Transaction tx = session.getTransaction();
        if (!tx.isActive()) {
            tx = session.beginTransaction();
        }

        var query = session.getEntityManagerFactory().createEntityManager()
                .createQuery("select f from Follow f where f.currentUserId=:curUserId and f.followingUser.userId=:followUserId", Follow.class);
        query.setParameter("curUserId", currentUserId);
        query.setParameter("followUserId", followingUserId);
        Follow followUser = query.getSingleResult();

        tx.commit();
        return followUser;
    }

    @Override
    public void deleteFollowingUser(Follow follow) {
        Session session = hibernateConfig.getSession();
        Transaction tx = session.getTransaction();
        if (!tx.isActive()) {
            tx = session.beginTransaction();
        }
        /*
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Reaction> criteriaQuery = cb.createQuery(Reaction.class);
        Root<Reaction> root = criteriaQuery.from(Reaction.class);
        criteriaQuery.where(cb.equal(root.get("reactId"), reaction.getReactId()));
        Reaction react = session.createQuery(criteriaQuery).getSingleResult();*/
        if (null != follow) {
            session.delete(follow);
        }
        tx.commit();
    }
}
