package org.spring.example.sharif.instragram.services;

import org.spring.example.sharif.instragram.dao.CommentDao;
import org.spring.example.sharif.instragram.dao.PostDao;
import org.spring.example.sharif.instragram.model.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService{

    @Autowired
    CommentDao commentDao;

    @Override
    public void addComment(Comment comment) {
        commentDao.addComment(comment);
    }

    @Override
    public List<Comment> getAllComments() {
        return commentDao.getAllComments();
    }

    @Override
    public List<Comment> getAllCommentsByPostId(long postId) {
        return commentDao.getAllCommentsByPostId(postId);
    }
}
