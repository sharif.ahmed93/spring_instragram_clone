package org.spring.example.sharif.instragram.services;

import org.spring.example.sharif.instragram.model.Post;
import org.spring.example.sharif.instragram.model.User;

import java.util.List;

public interface PostService {

    public void addPost(Post post);

    public User getPostByUserNameAndOwnerId(String username, long ownerId);

    public Post getPostByPostId(long postId);

    public User updatePostByUserIdAndPostId(long userId, long postId);

    public void updatePostByPostIdAndUserId(long postId, long userId);

    public List<Post> getAllPostByOwnerId(long postOwnerId);

    public List<Post> getAllPostByPostDateTime(String dateTime);

    public List<Post> getAllPost();
}
