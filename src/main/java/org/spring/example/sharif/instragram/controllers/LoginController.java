package org.spring.example.sharif.instragram.controllers;

import org.spring.example.sharif.instragram.model.User;
import org.spring.example.sharif.instragram.services.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class LoginController {

    @Autowired
    UserService userService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @GetMapping("/login")
    public String getLoginPage(Model model, @RequestParam(name="error",required = false) Boolean error){

        //generateUser();

        model.addAttribute("error",error);

        return "auth/login";
    }

    public void generateUser(){
        if (!userService.isUserExists("user")){
            User user = new User();
            user.setUsername("user");
            user.setPassword(passwordEncoder.encode("user"));
            userService.addUser(user);
        }
    }

    @GetMapping("/403")
    public String _403() {
        return "403";
    }

}
