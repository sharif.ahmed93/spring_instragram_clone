package org.spring.example.sharif.instragram.controllers;

import org.spring.example.sharif.instragram.dto.FollowDto;
import org.spring.example.sharif.instragram.model.Follow;
import org.spring.example.sharif.instragram.model.Post;
import org.spring.example.sharif.instragram.model.Reaction;
import org.spring.example.sharif.instragram.model.User;
import org.spring.example.sharif.instragram.services.FollowingService;
import org.spring.example.sharif.instragram.services.PostService;
import org.spring.example.sharif.instragram.services.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.net.BindException;

@Controller
public class FollowingController {

    @Autowired
    UserService userService;

    @Autowired
    PostService postService;

    @Autowired
    FollowingService followingService;

    @PostMapping("/user/follow/add")
    public String followUser(Model model, Authentication authentication, @ModelAttribute("followDto") FollowDto followDto, BindingResult bindingResult) {
        User currentUser = userService.getUserByUserName(authentication.getName());
        User followingUser = userService.getUserByUserId(followDto.getFollowingUserId());
        if (!bindingResult.hasErrors()) {
            Follow follow = new Follow();
            BeanUtils.copyProperties(followDto, follow);
            follow.setCurrentUserId(currentUser.getUserId());
            follow.setFollowingUser(followingUser);
            if (!followingService.alreadyFollowTheUserByCurrentUserId(currentUser.getUserId(), followingUser.getUserId())) {
                model.addAttribute("message", "You follow this people!");
                followingService.addFollowingUser(follow);
            } else {
                Follow followUser = followingService.getFollowingUserByCurrentUserId(currentUser.getUserId(), followingUser.getUserId());
                followingService.deleteFollowingUser(followUser);
                model.addAttribute("message", "You un follow this people!");
            }
            return "redirect:/index";
        }
        return "index";
    }
}
