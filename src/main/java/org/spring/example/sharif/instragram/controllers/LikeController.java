package org.spring.example.sharif.instragram.controllers;

import org.spring.example.sharif.instragram.dto.LikeDto;
import org.spring.example.sharif.instragram.model.Reaction;
import org.spring.example.sharif.instragram.model.Post;
import org.spring.example.sharif.instragram.model.User;
import org.spring.example.sharif.instragram.services.LikeService;
import org.spring.example.sharif.instragram.services.PostService;
import org.spring.example.sharif.instragram.services.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class LikeController {

    @Autowired
    UserService userService;

    @Autowired
    PostService postService;

    @Autowired
    LikeService likeService;

    @PostMapping("/user/post/like/add")
    public String addComment(Model model, Authentication authentication, @ModelAttribute("likeDto") @Valid LikeDto likeDto, BindingResult bindingResult) {
        Post post = postService.getPostByPostId(likeDto.getPostID());
        User user = userService.getUserByUserName(authentication.getName());
        if (!bindingResult.hasErrors()) {
            Reaction reaction = new Reaction();
            BeanUtils.copyProperties(likeDto, reaction);
            reaction.setUser(user);
            reaction.setPost(post);
            //likeService.addLikeToThePost(reaction);
            if (!likeService.alreadyLikeOfThisPostByPostId(post.getPostId(), user.getUserId())) {
                model.addAttribute("message", "You like this post!");
                likeService.addLikeToThePost(reaction);
            } else {
                Reaction react = likeService.getLikeOfThisPostByPostId(post.getPostId(), user.getUserId());
                likeService.deleteLikeToThePost(react);
                model.addAttribute("message", "You dislike this post!");
            }
            return "redirect:/index";
        }
        return "index";
    }

}

