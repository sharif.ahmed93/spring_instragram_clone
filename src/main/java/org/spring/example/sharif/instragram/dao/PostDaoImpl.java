package org.spring.example.sharif.instragram.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.spring.example.sharif.instragram.config.persistence.HibernateConfig;
import org.spring.example.sharif.instragram.model.Post;
import org.spring.example.sharif.instragram.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class PostDaoImpl implements PostDao {

    @Autowired
    HibernateConfig hibernateConfig;

    @Override
    public void addPost(Post post) {
        Session session = hibernateConfig.getSession();
        Transaction tx = session.getTransaction();

        if (!tx.isActive()) {
            tx = session.beginTransaction();
        }

        try {
            session.save(post);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();

        } finally {
            session.close();
        }
    }

    @Override
    public User getPostByUserNameAndOwnerId(String username, long ownerId) {
        return null;
    }

    @Override
    public Post getPostByPostId(long postId) {
        Session session = hibernateConfig.getSession();
        Transaction tx = session.getTransaction();

        if (!tx.isActive()) {
            tx = session.beginTransaction();
        }

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Post> userCriteriaQuery = cb.createQuery(Post.class);
        Root<Post> root = userCriteriaQuery.from(Post.class);

        userCriteriaQuery.where(cb.equal(root.get("postId"), postId));

        Post post = new Post();
        try {
            post = session.createQuery(userCriteriaQuery).getSingleResult();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }

        return post;
    }

    @Override
    public User updatePostByUserIdAndPostId(long userId, long postId) {
        return null;
    }

    @Override
    public void updatePostByPostIdAndUserId(long postId, long userId) {

    }

    @Override
    public List<Post> getAllPostByOwnerId(long postOwnerId) {
        Session session = hibernateConfig.getSession();
        Transaction tx = session.getTransaction();
        if (!tx.isActive()) {
            tx = session.beginTransaction();
        }
        //Team team = (Team) session.get(Team.class,id);
        /*CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Post> teamCriteriaQuery = cb.createQuery(Post.class);
        Root<Post> root = teamCriteriaQuery.from(Post.class);

        teamCriteriaQuery.where(cb.equal(root.get("user.userId"), postOwnerId));
        List<Post> posts = session.createQuery(teamCriteriaQuery).getResultList();*/

        var query = session.getEntityManagerFactory().createEntityManager()
                .createQuery("select p from Post p where p.user.userId=:uId ", Post.class);
        query.setParameter("uId", postOwnerId);
        List<Post> posts = query.getResultList();

        tx.commit();
        return posts;
    }

    @Override
    public List<Post> getAllPostByPostDateTime(String dateTime) {
        return null;
    }

    @Override
    public List<Post> getAllPost() {
        Session session = hibernateConfig.getSession();
        Transaction tx = session.getTransaction();
        if (!tx.isActive()) {
            tx = session.beginTransaction();
        }
        List<Post> postList = session.createQuery("from Post").list();
        tx.commit();
        return postList;
    }
}
