package org.spring.example.sharif.instragram.dao;

import org.spring.example.sharif.instragram.model.User;

import java.util.List;

public interface UserDao {
    public void addUser(User user);
    public boolean isUserExists(String username);
    public User getUserByUserName(String username);
    public User getUserByUserId(long userId);
    public void updateUserByUserId(User user);
    public List<User> getAllUsers();
}
