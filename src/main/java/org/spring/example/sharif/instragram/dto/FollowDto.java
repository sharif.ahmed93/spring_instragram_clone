package org.spring.example.sharif.instragram.dto;

import org.spring.example.sharif.instragram.model.User;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

public class FollowDto {
    private long followId;
    private long currentUserId;
    private long followingUserId;

    public long getFollowId() {
        return followId;
    }

    public void setFollowId(long followId) {
        this.followId = followId;
    }

    public long getCurrentUserId() {
        return currentUserId;
    }

    public void setCurrentUserId(long currentUserId) {
        this.currentUserId = currentUserId;
    }

    public long getFollowingUserId() {
        return followingUserId;
    }

    public void setFollowingUserId(long followingUserId) {
        this.followingUserId = followingUserId;
    }
}
