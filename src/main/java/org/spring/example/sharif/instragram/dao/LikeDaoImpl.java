package org.spring.example.sharif.instragram.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.spring.example.sharif.instragram.config.persistence.HibernateConfig;
import org.spring.example.sharif.instragram.model.Reaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class LikeDaoImpl implements LikeDao {

    @Autowired
    HibernateConfig hibernateConfig;

    @Override
    public void addLikeToThePost(Reaction reaction) {
        Session session = hibernateConfig.getSession();
        Transaction tx = session.getTransaction();

        if (!tx.isActive()) {
            tx = session.beginTransaction();
        }

        try {
            session.save(reaction);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();

        } finally {
            session.close();
        }
    }

    @Override
    public int getTotalLikeOfThisPostByPostId(long postId) {
        Session session = hibernateConfig.getSession();
        Transaction tx = session.getTransaction();
        if (!tx.isActive()) {
            tx = session.beginTransaction();
        }
        //Team team = (Team) session.get(Team.class,id);
        /*CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Comment> teamCriteriaQuery = cb.createQuery(Comment.class);
        Root<Comment> root = teamCriteriaQuery.from(Comment.class);

        teamCriteriaQuery.where(cb.equal(root.get("postId"), postId));
        List<Comment> comments = session.createQuery(teamCriteriaQuery).getResultList();
        */

        var query = session.getEntityManagerFactory().createEntityManager()
                .createQuery("select l from Reaction l where l.post.postId=:postId ", Reaction.class);
        query.setParameter("postId", postId);
        List<Reaction> reactions = query.getResultList();

        tx.commit();
        return reactions.size();
    }

    @Override
    public boolean alreadyLikeOfThisPostByPostId(long postId, long userId) {
        Session session = hibernateConfig.getSession();
        Transaction tx = session.getTransaction();
        if (!tx.isActive()) {
            tx = session.beginTransaction();
        }
        //Team team = (Team) session.get(Team.class,id);
        /*CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Comment> teamCriteriaQuery = cb.createQuery(Comment.class);
        Root<Comment> root = teamCriteriaQuery.from(Comment.class);

        teamCriteriaQuery.where(cb.equal(root.get("postId"), postId));
        List<Comment> comments = session.createQuery(teamCriteriaQuery).getResultList();
        */

        var query = session.getEntityManagerFactory().createEntityManager()
                .createQuery("select l from Reaction l where l.post.postId=:postId and l.user.userId=:uId", Reaction.class);
        query.setParameter("postId", postId);
        query.setParameter("uId", userId);
        List<Reaction> reactions = query.getResultList();

        tx.commit();
        return reactions.size() > 0 ? true : false;
    }

    @Override
    public Reaction getLikeOfThisPostByPostId(long postId, long userId) {
        Session session = hibernateConfig.getSession();
        Transaction tx = session.getTransaction();
        if (!tx.isActive()) {
            tx = session.beginTransaction();
        }

        var query = session.getEntityManagerFactory().createEntityManager()
                .createQuery("select l from Reaction l where l.post.postId=:postId and l.user.userId=:uId", Reaction.class);
        query.setParameter("postId", postId);
        query.setParameter("uId", userId);
        Reaction reaction = query.getSingleResult();

        tx.commit();
        return reaction;
    }

    @Override
    public void deleteLikeToThePost(Reaction reaction) {
        Session session =  hibernateConfig.getSession();
        Transaction tx = session.getTransaction();
        if (!tx.isActive()){
            tx = session.beginTransaction();
        }
        /*
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Reaction> criteriaQuery = cb.createQuery(Reaction.class);
        Root<Reaction> root = criteriaQuery.from(Reaction.class);
        criteriaQuery.where(cb.equal(root.get("reactId"), reaction.getReactId()));
        Reaction react = session.createQuery(criteriaQuery).getSingleResult();*/
        if(null != reaction){
            session.delete(reaction);
        }
        tx.commit();
    }
}
