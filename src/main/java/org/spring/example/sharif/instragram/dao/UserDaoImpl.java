package org.spring.example.sharif.instragram.dao;

import org.spring.example.sharif.instragram.config.persistence.HibernateConfig;
import org.spring.example.sharif.instragram.model.Comment;
import org.spring.example.sharif.instragram.model.Follow;
import org.spring.example.sharif.instragram.model.User;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class UserDaoImpl implements UserDao {

    @Autowired
    HibernateConfig hibernateConfig;

    @Override
    public void addUser(User user) {
        Session session = hibernateConfig.getSession();
        Transaction tx = session.getTransaction();

        if (!tx.isActive()) {
            tx = session.beginTransaction();
        }

        try {
            session.save(user);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();

        } finally {
            session.close();
        }
    }

    @Override
    public boolean isUserExists(String username) {

        Session session = hibernateConfig.getSession();
        Transaction tx = session.getTransaction();

        if (!tx.isActive()) tx = session.beginTransaction();

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<User> userCriteriaQuery = cb.createQuery(User.class);
        Root<User> root = userCriteriaQuery.from(User.class);

        userCriteriaQuery.where(cb.equal(root.get("username"), username));

        ArrayList<User> userList = new ArrayList<User>();
        try {
            userList = (ArrayList<User>) session.createQuery(userCriteriaQuery).getResultList();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }


        return userList.size() > 0 ? true : false;
    }

    @Override
    public User getUserByUserName(String username) {
        Session session = hibernateConfig.getSession();
        Transaction tx = session.getTransaction();

        if (!tx.isActive()) {
            tx = session.beginTransaction();
        }

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<User> userCriteriaQuery = cb.createQuery(User.class);
        Root<User> root = userCriteriaQuery.from(User.class);

        userCriteriaQuery.where(cb.equal(root.get("username"), username));

        User user = new User();
        try {
            user = session.createQuery(userCriteriaQuery).getSingleResult();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }

        return user;
    }

    @Override
    public User getUserByUserId(long userId) {
        Session session = hibernateConfig.getSession();
        Transaction tx = session.getTransaction();

        if (!tx.isActive()) {
            tx = session.beginTransaction();
        }

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<User> userCriteriaQuery = cb.createQuery(User.class);
        Root<User> root = userCriteriaQuery.from(User.class);

        userCriteriaQuery.where(cb.equal(root.get("userId"), userId));

        User user = new User();
        try {
            user = session.createQuery(userCriteriaQuery).getSingleResult();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }

        return user;
    }

    @Override
    public void updateUserByUserId(User user) {
        Session session = hibernateConfig.getSession();
        Transaction tx = session.getTransaction();
        if (!tx.isActive()) {
            tx = session.beginTransaction();
        }
        session.update(user);
        tx.commit();
    }

    @Override
    public List<User> getAllUsers() {
        Session session = hibernateConfig.getSession();
        Transaction tx = session.getTransaction();
        if (!tx.isActive()){
            tx = session.beginTransaction();
        }
        List<User> userList = session.createQuery("from User").list();
        tx.commit();
        return userList;
    }
}
