package org.spring.example.sharif.instragram.dto;

public class LikeDto {

    private long postID;

    public long getPostID() {
        return postID;
    }

    public void setPostID(long postID) {
        this.postID = postID;
    }
}
