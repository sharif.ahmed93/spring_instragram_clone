package org.spring.example.sharif.instragram.dao;

import org.spring.example.sharif.instragram.model.Comment;
import org.spring.example.sharif.instragram.model.Post;
import org.spring.example.sharif.instragram.model.User;

import java.util.List;

public interface CommentDao {

    public void addComment(Comment comment);

    public List<Comment> getAllComments();

    public List<Comment> getAllCommentsByPostId(long postId);

}
