package org.spring.example.sharif.instragram.services;

import org.spring.example.sharif.instragram.model.Comment;

import java.util.List;

public interface CommentService {

    public void addComment(Comment comment);

    public List<Comment> getAllComments();

    public List<Comment> getAllCommentsByPostId(long postId);
}
