package org.spring.example.sharif.instragram.controllers;

import org.spring.example.sharif.instragram.dto.UserDto;
import org.spring.example.sharif.instragram.model.User;
import org.spring.example.sharif.instragram.services.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class UserController {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UserService userService;


    @GetMapping("/user/register")
    public String getRegisterPage(Model model) {
        model.addAttribute("userDto", new UserDto());
        return "auth/register";
    }

    @PostMapping("/user/add")
    public String registerUser(Model model, @ModelAttribute("userDto") @Valid UserDto userDto, BindingResult bindingResult) {

        if (!bindingResult.hasErrors()) {
            if (!userService.isUserExists(userDto.getUsername())) {
                User user = new User();
                userDto.setPassword(passwordEncoder.encode(userDto.getPassword()));
                BeanUtils.copyProperties(userDto, user);
                userService.addUser(user);
                model.addAttribute("message","User register successfully done!");
                return "auth/login";
            } else {
                model.addAttribute("message","User already exist of this user name");
                return "auth/register";
            }
        }
        return "auth/register";
    }

}
