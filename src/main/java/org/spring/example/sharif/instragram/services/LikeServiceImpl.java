package org.spring.example.sharif.instragram.services;

import org.spring.example.sharif.instragram.dao.LikeDao;
import org.spring.example.sharif.instragram.model.Reaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LikeServiceImpl implements LikeService {

    @Autowired
    LikeDao likeDao;

    @Override
    public void addLikeToThePost(Reaction reaction) {
        likeDao.addLikeToThePost(reaction);
    }

    @Override
    public int getTotalLikeOfThisPostByPostId(long postId) {
        return likeDao.getTotalLikeOfThisPostByPostId(postId);
    }

    @Override
    public boolean alreadyLikeOfThisPostByPostId(long postId, long userId) {
        return likeDao.alreadyLikeOfThisPostByPostId(postId, userId);
    }

    @Override
    public Reaction getLikeOfThisPostByPostId(long postId, long userId) {
        return likeDao.getLikeOfThisPostByPostId(postId,userId);
    }

    @Override
    public void deleteLikeToThePost(Reaction reaction) {
        likeDao.deleteLikeToThePost(reaction);
    }

}
