package org.spring.example.sharif.instragram.services;

import org.spring.example.sharif.instragram.model.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public interface UserService extends UserDetailsService {
    public void addUser(User user);
    public boolean isUserExists(String username);
    public User getUserByUserName(String username);
    public User getUserByUserId(long userId);
    public void updateUserByUserId(User user);
    public List<User> getAllUsers();
    @Override
    public UserDetails loadUserByUsername(String username);
}
