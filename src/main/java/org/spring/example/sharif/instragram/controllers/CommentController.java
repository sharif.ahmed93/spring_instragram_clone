package org.spring.example.sharif.instragram.controllers;


import org.spring.example.sharif.instragram.dto.CommentDto;
import org.spring.example.sharif.instragram.dto.PostDto;
import org.spring.example.sharif.instragram.model.Comment;
import org.spring.example.sharif.instragram.model.Post;
import org.spring.example.sharif.instragram.model.User;
import org.spring.example.sharif.instragram.services.CommentService;
import org.spring.example.sharif.instragram.services.PostService;
import org.spring.example.sharif.instragram.services.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class CommentController {

    @Autowired
    UserService userService;

    @Autowired
    PostService postService;

    @Autowired
    CommentService commentService;

    @PostMapping("/user/post/comment/add")
    public String addComment(Model model, Authentication authentication, @ModelAttribute("commentDto") @Valid CommentDto commentDto, BindingResult bindingResult) {
        Post post = postService.getPostByPostId(commentDto.getPostId());
        User user = userService.getUserByUserName(authentication.getName());
        if (!bindingResult.hasErrors()) {
            Comment comment = new Comment();
            BeanUtils.copyProperties(commentDto, comment);
            comment.setUser(user);
            comment.setPost(post);
            commentService.addComment(comment);
            model.addAttribute("message", "Comment added successfully done!");
            return "redirect:/index";
        }
        return "index";
    }



}
