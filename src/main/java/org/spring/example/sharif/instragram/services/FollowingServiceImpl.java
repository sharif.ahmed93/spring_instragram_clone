package org.spring.example.sharif.instragram.services;

import org.spring.example.sharif.instragram.dao.FollowingDao;
import org.spring.example.sharif.instragram.model.Follow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FollowingServiceImpl implements FollowingService {
    @Autowired
    FollowingDao followingDao;

    @Override
    public void addFollowingUser(Follow follow) {
        followingDao.addFollowingUser(follow);
    }

    @Override
    public List<Follow> getAllFollowingUserByCurrentUserId(long currentUserId) {
        return followingDao.getAllFollowingUserByCurrentUserId(currentUserId);
    }

    @Override
    public boolean alreadyFollowTheUserByCurrentUserId(long currentUserId, long followingUserId) {
        return followingDao.alreadyFollowTheUserByCurrentUserId(currentUserId,followingUserId);
    }

    @Override
    public Follow getFollowingUserByCurrentUserId(long currentUserId, long followingUserId) {
        return followingDao.getFollowingUserByCurrentUserId(currentUserId, followingUserId);
    }

    @Override
    public void deleteFollowingUser(Follow follow) {
        followingDao.deleteFollowingUser(follow);
    }
}
