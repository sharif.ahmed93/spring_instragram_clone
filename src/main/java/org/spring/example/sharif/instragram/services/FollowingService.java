package org.spring.example.sharif.instragram.services;

import org.spring.example.sharif.instragram.model.Follow;

import java.util.List;

public interface FollowingService {
    public void addFollowingUser(Follow follow);
    public List<Follow> getAllFollowingUserByCurrentUserId(long currentUserId);
    public boolean alreadyFollowTheUserByCurrentUserId(long currentUserId, long followingUserId);
    public Follow getFollowingUserByCurrentUserId(long currentUserId, long followingUserId);
    public void deleteFollowingUser(Follow follow);
}
