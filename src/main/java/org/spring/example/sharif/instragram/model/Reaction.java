package org.spring.example.sharif.instragram.model;

import javax.persistence.*;

@Entity
public class Reaction {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long reactId;
    @OneToOne
    @JoinColumn(name = "user_Id")
    private User user;
    @OneToOne
    @JoinColumn(name = "post_Id")
    private Post post;

    public long getReactId() {
        return reactId;
    }

    public void setReactId(long reactId) {
        this.reactId = reactId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }
}
