package org.spring.example.sharif.instragram.controllers;

import org.spring.example.sharif.instragram.dto.PostDto;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class RootController {
    @GetMapping("/")
    public String rootMap(Model model) {
        return "redirect:/index";
    }
    @GetMapping("/index")
    public String userHomePage(Model model) {
        //model.addAttribute("postDto",new PostDto());
        //return "index";
        return "redirect:/user/post/show/all";
    }
}
