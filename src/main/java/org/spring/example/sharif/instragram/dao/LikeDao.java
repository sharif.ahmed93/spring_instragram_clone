package org.spring.example.sharif.instragram.dao;

import org.spring.example.sharif.instragram.model.Reaction;

public interface LikeDao {

    public void addLikeToThePost(Reaction reaction);

    public int getTotalLikeOfThisPostByPostId(long postId);

    public boolean alreadyLikeOfThisPostByPostId(long postId,long userId);

    public Reaction getLikeOfThisPostByPostId(long postId,long userId);

    public void deleteLikeToThePost(Reaction reaction);
}
