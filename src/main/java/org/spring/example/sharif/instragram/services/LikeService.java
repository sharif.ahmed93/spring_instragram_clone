package org.spring.example.sharif.instragram.services;

import org.spring.example.sharif.instragram.model.Reaction;

public interface LikeService {
    public void addLikeToThePost(Reaction reaction);

    public int getTotalLikeOfThisPostByPostId(long postId);

    public boolean alreadyLikeOfThisPostByPostId(long postId,long userId);

    public Reaction getLikeOfThisPostByPostId(long postId,long userId);

    public void deleteLikeToThePost(Reaction reaction);
}
