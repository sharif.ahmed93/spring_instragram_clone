package org.spring.example.sharif.instragram.dto;

public class CommentDto {

    private long postId;
    private String commentDesc;

    public CommentDto() {

    }

    public long getPostId() {
        return postId;
    }

    public void setPostId(long postId) {
        this.postId = postId;
    }

    public String getCommentDesc() {
        return commentDesc;
    }

    public void setCommentDesc(String commentDesc) {
        this.commentDesc = commentDesc;
    }
}
