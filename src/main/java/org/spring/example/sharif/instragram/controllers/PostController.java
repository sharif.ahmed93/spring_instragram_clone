package org.spring.example.sharif.instragram.controllers;

import org.spring.example.sharif.instragram.Constants;
import org.spring.example.sharif.instragram.dto.CommentDto;
import org.spring.example.sharif.instragram.dto.PostDto;
import org.spring.example.sharif.instragram.model.Post;
import org.spring.example.sharif.instragram.model.User;
import org.spring.example.sharif.instragram.services.PostService;
import org.spring.example.sharif.instragram.services.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

@Controller
public class PostController {
    @Autowired
    UserService userService;

    @Autowired
    PostService postService;


    @PostMapping("/user/post/add")
    public String addPost(Model model,Authentication authentication, @ModelAttribute("postDto") @Valid PostDto postDto,MultipartFile file, BindingResult bindingResult) {
        User user = userService.getUserByUserName(authentication.getName());
        if (!bindingResult.hasErrors()) {
            Post post = new Post();
            BeanUtils.copyProperties(postDto, post);
            String postImageUrl = uploadFile(file);
            post.setUser(user);
            post.setPostImage(postImageUrl);
            postService.addPost(post);
            model.addAttribute("message", "Post added successfully done!");
            return "redirect:/index";
        }
        return "index";
    }



    public String uploadFile(MultipartFile file) {
        if (!file.isEmpty()) {
            try {
                String absolutePath = Constants.POST_IMG_ABSOLUTE_PATH;
                String fileName = "post" + new Date().getTime() + ".jpg";

                Path path = Paths.get(absolutePath);
                File directory = path.toFile();

                //servletContext.getRealPath("/WEB-INF/resources/images/profile/")
                //File dir = Paths.get(absoluteFilePath + userName + "//").toFile();

                if (!directory.exists()) {
                    boolean bool = directory.mkdirs();
                    if (bool) {
                        System.out.println("Directory created successfully");
                    } else {
                        System.out.println("Sorry couldn't create specified directory");
                    }
                }

                System.out.println("dir path: " + directory.getAbsolutePath());

                File outputFile = new File(directory, fileName);

                BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(outputFile));
                outputStream.write(file.getBytes());
                outputStream.flush();
                outputStream.close();

                //String outputImageUrl = directory.getAbsolutePath()+fileName;
                String outputImageUrl = "/images/post/"+fileName;
                System.out.println("file name: " + outputFile.getName());
                //return outputFile.getName();
                return outputImageUrl;
            } catch (IOException e) {
                e.printStackTrace();
                return "";
            }
        } else {
            return "";
        }
    }

}
