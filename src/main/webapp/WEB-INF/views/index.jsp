<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core_1_1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Home</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Instragram</a>
    <sec:authentication var="principal" property="principal"/>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
        </ul>
        <ul class="navbar-nav my-2 my-lg-0">
            <li class="nav-item">
                <form:form action="${pageContext.request.contextPath}/user/profile" method="get">
                    <button class="btn btn-outline-success my-2 my-sm-0"
                            type="submit">${principal.username}</button>
                </form:form>
            </li>
            <li class="nav-item" style="margin-left:10px ">
                <form:form action="${pageContext.request.contextPath}/logout" method="get">
                    <button class="btn btn-info my-2 my-sm-0"
                            type="submit">Logout
                    </button>
                </form:form>
            </li>
        </ul>
    </div>
</nav>
<div class="container-fluid">
    <br>
    <div class="row">
        <div class="col">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <%--@elvariable id="postDto" type="org.spring.example.sharif.instragram.dto.PostDto"--%>
                        <form:form class="form"
                                   action="${pageContext.request.contextPath}/user/post/add"
                                   method="post"
                                   id="registrationForm"
                                   modelAttribute="postDto"
                                   enctype="multipart/form-data">
                            <div class="card-header border-transparent">
                                <h6 class="card-title">Write Post</h6>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body p-0">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="postDesc">Post Desc</label>
                                        <form:textarea class="form-control"
                                                       id="postDesc"
                                                       rows="2"
                                                       path="postDesc"
                                        />
                                    </div>
                                    <div class="col-md-12">
                                        <label for="postImage">Post Image</label><br>
                                        <input type="file" name="file"
                                               id="postImage"
                                               class="text-center center-block file-upload"
                                        />
                                    </div>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer clearfix">
                                <button class="btn btn-lg btn-success pull-right" type="submit">
                                    Add Post
                                </button>
                            </div>
                        </form:form>
                    </div>
                </div>
                <br><br>
                <div class="col-12" style="margin-top: 10px">
                    <div class="card">
                        <%--@elvariable id="followingUserList" type="java.util.List"--%>
                        <%--@elvariable id="follow" type="org.spring.example.sharif.instragram.model.Follow"--%>

                        <div class="card-header">
                            <h3 class="card-title">You are following ${followingUserList.size()} peoples</h3>

                            <div class="card-tools">
                                <%--<span class="badge badge-danger">${followingUserList.size()} New Members</span>--%>
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove"><i
                                        class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-0">
                            <ul class="users-list clearfix" style="list-style: none">
                                <c:forEach items="${followingUserList}" var="follow">
                                    <li>
                                        <div class="row" align="center">
                                            <div class="col" align="center">
                                                <img src="${pageContext.request.contextPath}${follow.followingUser.profileImage}"
                                                     width="50" height="50"
                                                     alt="User Image">
                                            </div>
                                            <div class="col" align="center">
                                                <h6>${follow.followingUser.fullName}</h6>
                                            </div>
                                        </div>
                                    </li>
                                </c:forEach>
                            </ul>
                            <!-- /.users-list -->
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer text-center">
                            <a href="javascript">View All Users</a>
                        </div>
                        <!-- /.card-footer -->
                    </div>
                </div>
            </div>

        </div>
        <div class="col-6">
            <div class="card">
                <div class="card-header">
                    <h6 class="card-title">See All Latest Post</h6>

                    <div class="card-tools">
                        <span class="badge badge-danger">${postList.size()} New Posts</span>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body" style="margin-top: 10px">
                    <ul style="list-style: none">
                        <li>
                            <%--@elvariable id="postList" type="java.util.List"--%>
                            <%--@elvariable id="post" type="org.spring.example.sharif.instragram.model.Post"--%>
                            <c:forEach items="${postList}" var="post">
                                <div class="card">
                                    <div class="card-body">
                                        <img alt="post image" class="card-img" height="600"
                                             src="${pageContext.request.contextPath}${post.postImage}"
                                            <%--src="https://assets.breatheco.de/apis/img/images.php?blob&tags=bobdylan"--%>
                                        />
                                        <p>${post.postDesc}</p>
                                    </div>
                                    <div class="card-footer clearfix">
                                            <%--@elvariable id="likeDto" type="org.spring.example.sharif.instragram.dto.LikeDto"--%>
                                        <div class="row" align="center">
                                            <form:form
                                                    action="${pageContext.request.contextPath}/user/post/like/add"
                                                    modelAttribute="likeDto"
                                                    method="post">
                                                <div class="col-md-2" align="center">
                                                    <form:input type="hidden" path="postID" value="${post.postId}"/>
                                                    <button type="submit" class="btn btn-secondary">Like</button>
                                                </div>
                                            </form:form>
                                            <div class="col-md-2" align="center">
                                                    <%--@elvariable id="totalLikeList" type="java.util.List"--%>
                                                <p>${totalLikeList.get(postList.indexOf(post))} Likes</p>
                                            </div>
                                            <div class="col-md-6" align="center">
                                                    <%--@elvariable id="commentDto" type="org.spring.example.sharif.instragram.dto.CommentDto"--%>
                                                <form:form class="form"
                                                           action="${pageContext.request.contextPath}/user/post/comment/add"
                                                           method="post"
                                                           modelAttribute="commentDto">
                                                    <div class="input-group mb-3" align="center">
                                                        <form:input type="text" class="form-control"
                                                                    placeholder="Write your comment here...."
                                                                    aria-describedby="basic-addon2" path="commentDesc"/>
                                                        <div class="input-group-append" align="center">
                                                            <form:input type="hidden" path="postId"
                                                                        value="${post.postId}"/>
                                                            <button class="btn btn-outline-secondary" type="submit">
                                                                Comment
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form:form>
                                            </div>
                                            <div class="col-md-2">
                                                    <%--@elvariable id="totalCommentList" type="java.util.List"--%>
                                                <p>${totalCommentList.get(postList.indexOf(post))} Comments</p>
                                            </div>
                                        </div>
                                        <h6>See all comments</h6>
                                        <table class="table m-0">
                                            <tbody>
                                                <%--@elvariable id="commentList" type="java.util.List"--%>
                                                <%--@elvariable id="comment" type="org.spring.example.sharif.instragram.model.Comment"--%>
                                            <c:forEach items="${commentList.get(postList.indexOf(post))}" var="comment">
                                                <tr>
                                                    <div class="row">
                                                        <div class="col-3">
                                                            <div class="row" align="center-left">
                                                                <div class="col-12" align="left">
                                                                    <img align="left" alt="User Image" width="25"
                                                                         height="25"
                                                                         src="${pageContext.request.contextPath}${comment.user.profileImage}"/>
                                                                </div>
                                                                <div class="col-12">
                                                                        ${comment.user.fullName}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-9">
                                                            <p>${comment.commentDesc}</p>
                                                        </div>
                                                    </div>

                                                </tr>
                                                <hr>
                                            </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <br>
                            </c:forEach>
                        </li>

                    </ul>
                    <!-- /.users-list -->
                </div>
                <!-- /.card-body -->
                <div class="card-footer text-center">
                    <a href="javascript:">See All Posts</a>
                </div>
                <!-- /.card-footer -->
            </div>
        </div>
        <div class="col">
            <div class="card">
                <%--@elvariable id="userList" type="java.util.List"--%>
                <%--@elvariable id="user" type="org.spring.example.sharif.instragram.model.User"--%>
                <div class="card-header">
                    <h3 class="card-title">People you may know</h3>

                    <div class="card-tools">
                        <span class="badge badge-danger">${userList.size()} New Members</span>
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove"><i
                                class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                    <ul class="users-list clearfix" style="list-style: none">
                        <c:forEach items="${userList}" var="user">
                            <li>
                                <div class="row" align="center" style="margin-top: 10px">
                                    <div class="col-12" align="center">
                                        <img src="${pageContext.request.contextPath}${user.profileImage}"
                                             alt="User Profile Image" class="img-thumbnail" width="50%" height="50%">
                                    </div>
                                    <div class="col-12" align="center">
                                        <h6 style="margin-top: 5px">${user.fullName}</h6>
                                            <%--@elvariable id="followDto" type="org.spring.example.sharif.instragram.dto.FollowDto"--%>
                                        <form:form action="${pageContext.request.contextPath}/user/follow/add"
                                                   method="post" modelAttribute="followDto">
                                            <form:input type="hidden" path="followingUserId" value="${user.userId}"/>
                                            <button type="submit" class="btn btn-primary">Follow</button>
                                        </form:form>
                                    </div>
                                </div>
                            </li>
                            <br>
                        </c:forEach>
                    </ul>
                    <!-- /.users-list -->
                </div>
                <!-- /.card-body -->
                <div class="card-footer text-center">
                    <a href="javascript:">View All Users</a>
                </div>
                <!-- /.card-footer -->
            </div>
        </div>
    </div>

</div>

</body>
</html>
